if (!Array.prototype.filter) {
  // The MDN version is weird and can end up in an infinite loop for god knows why,
  // and is also really really bad in terms of code style, so here's my version
  // -D Plaza

  Array.prototype.filter = function (func, thisArg) {
    'use strict'

    if ( !((typeof func).toLowerCase() === 'function') && this) {
      throw new TypeError()
    }

    var len = this.length >>> 0
    // preallocate array
    var res = new Array(len)
    // Save the current array
    var arr = this
    // our current position in the result array
    var cur = 0

    for (var i = 0; i < arr.length; i++) {
      if (!(i in arr)) {
        // Skip entries not in the array
        continue
      }

      if (thisArg === undefined) {
        if (func(arr[i], i, arr))
          res[cur++] = arr[i]
      } else {
        if (func.call(thisArg, arr[i], i, arr))
          res[cur++] = arr[i]
      }
    }

    // shrink down array to proper size
    res.length = cur;
    return res;
  };
}
