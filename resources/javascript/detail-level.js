"use strict"
// Everything is inside an IIFE because scoping
// Now nobody can access the internals, muahaha
;(function() {
  "use strict"
  /** The slider element */
  var slider
  /** The <main> tag in the page */
  var mainPage
  /** Used to detect if input is supported as an event */
  var supportsInput = false

  /**
   * Handle a slider change and change the detail level
   * @param {Event} e The event
   */
  function sliderChange(e) {
    e = e || window.event
    var target = e.target || e.srcElement

    // """type casting""" to a number :)
    changeDetailTo(+target.value)
  }

  /**
   * Change the detail level of the current document to a new level
   * Ranges from 1-3, with 1 being lowest detail and 3 being the most
   * @param {Number} level The level to change it to
   */
  function changeDetailTo(level) {
    level = parseInt(level)

    // Checking for NaN
    if (level !== level) {
      throw new Error("Invalid input: " + level + " is not a number")
    }

    var hide = []

    if (level === 1) {
      hide.push(2)
      hide.push(3)
    } else if (level === 2) {
      hide.push(3)
    }

    var hideClasses = hide.map(function (v) { return " hide-detail-" + v })

    mainPage.className = hideClasses.join(" ")
  }

  /**
   * Create a slider, append it to the given container, and then return it
   * @param {Element} container The elements that holds the slider
   * @returns {Element} The created slider
   */
  function createSlider(container) {
    /*
      <label>Level of detail: <br/>
        Min <input type="range" min="1" max="3" step="1"/> Max
      </label>
    */

    var label = document.createElement("label")
    U.setText(label, "Level of detail: ")

    var slider = document.createElement("input")
    slider.setAttribute("type", "range")
    slider.setAttribute("min", "1")
    slider.setAttribute("max", "3")
    slider.setAttribute("step", "1")

    label.appendChild(document.createElement("br"))
    label.appendChild(document.createTextNode("Min "))
    label.appendChild(slider)
    label.appendChild(document.createTextNode(" Max"))

    container.appendChild(label)

    return slider
  }

  /**
   * Init the detail slider code, essentially make it visible and add a handler
   */
  function init() {
    // Check if we support input before trying to use it
    if (!U.supportsInput("range")) {
      return
    }

    var sliderContainer = document.getElementsByClassName("detail-slider-container")[0]

    slider = createSlider(sliderContainer)
    mainPage = document.getElementsByTagName("main")[0]

    if (slider) {
      U.addHandler(slider, "input", function(e) {
        supportsInput = true
        sliderChange(e)
      })
      U.addHandler(slider, "change", function(e) {
        // Input fires before change, so if supportsInput is true then we support input,
        // avoid double changes
        if (!supportsInput) {
          sliderChange(e)
        }
        // I learn to hate IE more every day
      })

      slider.value = 1
      changeDetailTo(+slider.value)
    }
  }

  U.ready(init)
})()
